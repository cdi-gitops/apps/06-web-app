Übung(en): Tests und Qualitätsüberprüfungen
===========================================


Einfache Web Applikation - Erweiterungen und Tests

Maven Erweiterungen
-------------------

Erweitert die Build Datei `pom.xml` um:

    <plugin>
        <groupId>org.jacoco</groupId>
        <artifactId>jacoco-maven-plugin</artifactId>
        <version>0.8.2</version>
        <executions>
            <execution>
                <goals>
                    <goal>prepare-agent</goal>
                </goals>
            </execution>
            <execution>
                <id>report</id>
                <phase>prepare-package</phase>
                <goals>
                    <goal>report</goal>
                </goals>
            </execution>
        </executions>
    </plugin>

* [Intro to JaCoCo](https://www.baeldung.com/jacoco)

GitLab CI/CD Jobs
-----------------    

Erweitert die `.gitlab-ci.yml` Datei um folgende Einträge:

    stages:
        - build
        ...
        - test
    
        ...
    test_unit_job:
      stage: test
      script:
        - mvn verify
      artifacts:
        when: always
        reports:
          junit:
            - target/surefire-reports/TEST-*.xml
            - target/failsafe-reports/TEST-*.xml
    
    test-jdk11:
      stage: test
      image: maven:3.6.3-jdk-11
      script:
        - mvn $MAVEN_CLI_OPTS clean org.jacoco:jacoco-maven-plugin:prepare-agent test jacoco:report
      artifacts:
        paths:
          - target/site/jacoco/jacoco.xml
    
    coverage-jdk11:
      # Must be in a stage later than test-jdk11's stage.
      # The `visualize` stage does not exist by default.
      # Please define it first, or choose an existing stage like `deploy`.
      stage: visualize
      image: registry.gitlab.com/haynes/jacoco2cobertura:1.0.7
      script:
        # convert report from jacoco to cobertura, using relative project path
        - python /opt/cover2cover.py target/site/jacoco/jacoco.xml $CI_PROJECT_DIR/src/main/java/ > target/site/cobertura.xml
      needs: ["test-jdk11"]
      artifacts:
        reports:
          coverage_report:
            coverage_format: cobertura
            path: target/site/cobertura.xml     
    
    include:
      - template: Code-Quality.gitlab-ci.yml   
      - template: Security/SAST.gitlab-ci.yml          


Einbau Schwachstelle(n) in Code
-------------------------------

Editiert die Datei [Main.java](https://gitlab.com/cdi-work/apps/05-web-app/-/blob/master/src/main/java/ch/cdi/webapp/Main.java) und fügt folgende Zeile, hinter `BASE_URI`, hinzu:

    public static final String DEFAULT_USER     = "Administrator";
    public static final String DEFAULT_PASSWORD = "geheim";

Einbau Testfehler
-----------------

Zu Testzwecken lassen wir einen Unit Tests fehlschlagen.

Dazu ändert die Datei [MyResource.java](https://gitlab.com/cdi-work/apps/05-web-app/-/blob/master/src/main/java/ch/cdi/webapp/MyResource.java), damit sie einen anderen Wert zurück liefert.

alt:

    return "Got it!";

neu:

    return "alles Gut!";


